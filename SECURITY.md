# Security Policy

## Supported Versions

These are our versions that we support

| Version | Supported          |
| ------- | ------------------ |
| 1.0.7   | :white_check_mark: |

## Reporting a Vulnerability

Please pm Lil Durk#6400
